﻿namespace AdventOfCode {
    /// <summary>
    /// It's just three-dimensional arrays bro
    /// </summary>
    public static class Day9 {
        public static long Day9_1() {
            var Data = LoadData();
            foreach(var item in Data) {
                Extrapolate(item);
                for (int i = item.Count() - 2; i >= 0; i--) {
                    item[i].Add(item[i].Last() + item[i + 1].Last());
                }
            }
            return Data.Select(x => x.First().Last()).Sum();
        }

        public static long Day9_2() {
            var Data = LoadData();
            foreach (var item in Data) {
                Extrapolate(item);
                for (int i = item.Count() - 2; i >= 0; i--) {
                    item[i].Insert(0, item[i].First() - item[i + 1].First());
                }
            }
            return Data.Select(x => x.First().First()).Sum();
        }

        private static void Extrapolate(List<List<long>> item) {
            while (!(item.Last().First() == 0 && item.Last().Distinct().Count() == 1)) {
                List<long> list = new List<long>();
                for (var i = 0; i < item.Last().Count() - 1; i++) {
                    list.Add(item.Last()[i + 1] - item.Last()[i]);
                }
                item.Add(list);
            }
        }

        private static List<List<List<long>>> LoadData() {
            string[] lines = File.ReadAllLines("input/Day9");
            return lines.Select(x => new List<List<long>>() { x.Split(" ").Select(x => long.Parse(x)).ToList() }).ToList();
        }
    }
}