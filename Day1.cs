﻿using System.Text.RegularExpressions;

namespace AdventOfCode {
    public partial class Day1 {
        public static int Trebuchet1() {
            string[] lines = File.ReadAllLines("input/Day1");
            int total = 0;

            foreach (string line in lines) {
                var m = Trebuchet1Regex().Matches(line);
                var firstNum = m.First().Groups[1].Value;
                var secondNum = m.Last().Groups[1].Value;
                var item = int.Parse(firstNum + secondNum);
                total += item;
            }

            return total;
        }

        public static int Trebuchet2() {
            string[] lines = File.ReadAllLines("input/Day1");
            int total = 0;
            Regex R = Trebuchet2Regex();
            Dictionary<string, string> map = new(){
                {"one", "1" }, {"two", "2" }, {"three",  "3" }, {"four", "4" }, {"five", "5"}, {"six", "6"}, {"seven", "7"}, {"eight", "8"}, {"nine", "9"}
            };

            foreach (string line in lines) {
                var m = R.Matches(line);

                var firstNumVal = m.First().Groups[1].Value;
                var secondNumVal = m.Last().Groups[1].Value;

                var item = int.Parse(
                    (map.TryGetValue(firstNumVal, out string? val) ? val : firstNumVal) +
                    (map.TryGetValue(secondNumVal, out string? val2) ? val2 : secondNumVal)
                );
                //Console.WriteLine(line + " - " + firstNumVal + ":" + secondNumVal + ":" + item);
                total += item;
            }

            return total;
        }

        [GeneratedRegex(@"(\d)")]
        private static partial Regex Trebuchet1Regex();

        [GeneratedRegex("(?=(\\d|one|two|three|four|five|six|seven|eight|nine))")]
        private static partial Regex Trebuchet2Regex();
    }
}
