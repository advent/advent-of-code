﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AdventOfCode {
    public static partial class Day7 {
        public static long Day7_1() => ProcessHands();
        public static long Day7_2() => ProcessHands(true);

        private static long ProcessHands(bool UseJokers = false) {
            var Hands = LoadHands();
            foreach (var h in Hands) {
                h.ScoreCards(UseJokers);
            }

#pragma warning disable IDE0305 // Simplify collection initialization. fucking stupid suggestion holy shit
            Hands = Hands
                .OrderBy(x => x.Score)
                .ThenBy(x => x.CardScores[0])
                .ThenBy(x => x.CardScores[1])
                .ThenBy(x => x.CardScores[2])
                .ThenBy(x => x.CardScores[3])
                .ThenBy(x => x.CardScores[4])
                .ToList();
#pragma warning restore IDE0305 // Simplify collection initialization

            return Hands.Select((x, i) => (i + 1) * x.Bid).Sum();
        }

        private static List<Hand> LoadHands() {
            string[] lines = File.ReadAllLines("input/Day7");
            Regex r = CardParser();
            return lines.Select(x => {
                var m = r.Match(x);
                return new Hand {
                    Cards = m.Groups[1].Value,
                    Bid = int.Parse(m.Groups[2].Value)
                };
            }).ToList();
        }

        private static Dictionary<string, int> CardScoreMap = new() {
            {"A",14 },
            {"K",13 },
            {"Q",12 },
            {"J",11 },
            {"T",10 },
            {"9",9 },
            {"8",8 },
            {"7",7 },
            {"6",6 },
            {"5",5 },
            {"4",4 },
            {"3",3 },
            {"2",2 },
        };

        public static class Scores {
            public const int FiveOfAKind = 20;
            public const int FourOfAKind = 19;
            public const int FullHouse = 18;
            public const int ThreeOfAKind = 17;
            public const int TwoPair = 16;
            public const int OnePair = 15;
        }

        public class Hand {
            public required string Cards;
            public int Bid;
            public int Score;

            public List<int> CardScores = [];

            public void ScoreCards(bool UseJokers = false) {
                if (UseJokers) {
                    CardScoreMap["J"] = 1;
                }
                CardScores = Cards
                    .ToCharArray()
                    .Select(x => CardScoreMap[x.ToString()]).ToList();
                var CardValues = CardScores.Where(x => x > 1).GroupBy(p => p, p => p, (key, g) => new { key, cards = g.Count() })
                    .Select(x => new GroupByVal() {
                        key = x.key,
                        cards = x.cards
                    })
                    .ToList();
                int JokerCount = 0;
                if (UseJokers) {
                    JokerCount = CardScores.Where(x => CardScoreMap["J"] == x).Count();
                }
                if (CardValues.Any(x => x.cards + JokerCount == 5) || JokerCount == 5) {
                    Score = Scores.FiveOfAKind;
                } else if (CardValues.Any(x => x.cards + JokerCount == 4)) {
                    Score = Scores.FourOfAKind;
                } else if (CalculateFullHouse(CardValues, JokerCount, 3, 2)) {
                    Score = Scores.FullHouse;
                } else if (CardValues.Any(x => x.cards + JokerCount == 3)) {
                    Score = Scores.ThreeOfAKind;
                } else if (CalculateFullHouse(CardValues, JokerCount, 2, 2)) {
                    Score = Scores.TwoPair;
                } else if (CardValues.Any(x => x.cards + JokerCount == 2)) {
                    Score = Scores.OnePair;
                } else {
                    Score = 0;
                }
            }

            private static bool CalculateFullHouse(List<GroupByVal> cardValues, int jokerCount, int FirstVal, int SecondVal) {
                var First = cardValues.OrderByDescending(x => x.cards).ElementAt(0);
                if ((First.cards == FirstVal && jokerCount == SecondVal) || (First.cards == SecondVal && jokerCount == FirstVal)) return true;
                var Second = cardValues.OrderByDescending(x => x.cards).ElementAt(1);

                var FirstUsedJokers = (FirstVal - First.cards);
                return Second.cards + jokerCount - FirstUsedJokers == SecondVal;
            }

            private class GroupByVal {
                public int key;
                public int cards;
            }
        }

        [GeneratedRegex("(.+) (.+)")]
        private static partial Regex CardParser();
    }
}
