﻿using System.Text.RegularExpressions;

namespace AdventOfCode {
    /// <summary>
    /// It's just x-intercepts of a quadratic expression bro
    /// </summary>
    public static partial class Day6 {

        public static long Day6_1() {
            var RaceData = LoadData();

            long Total = 1;
            foreach (var Race in RaceData) {
                Total *= Race.GetPossibilities();
            }
            return Total;
        }
        public static long Day6_2() {
            var data = LoadData();
            RaceData R = new() {
                Time = long.Parse(String.Join("", data.Select(x => x.Time.ToString()))),
                Distance = long.Parse(String.Join("", data.Select(x => x.Distance.ToString())))
            };
            return R.GetPossibilities();
        }

        private static List<RaceData> LoadData() {
            string[] lines = File.ReadAllLines("input/Day6");
            Regex getNums = RaceParser();
            List<int> Times = getNums.Matches(lines[0]).Select(i => int.Parse(i.Value)).ToList();
            List<int> Distances = getNums.Matches(lines[1]).Select(i => int.Parse(i.Value)).ToList();
            List<RaceData> Races = [];
            for(int i = 0; i < Times.Count; i++) {
                Races.Add(new RaceData {
                    Time = Times[i],
                    Distance = Distances[i]
                });
            }
            return Races;
        }

        public class RaceData {
            public long Time;
            public long Distance;

            public long GetPossibilities() {
                var min = (long)Math.Ceiling(GetValue(-1));
                var max = (long)Math.Floor(GetValue(1));
                return max - min + 1;
            }

            private double GetValue(int PosNeg) {
                return (Time + (PosNeg * Math.Sqrt(Math.Pow(Time, 2) - (4 * 1 * Distance)))) / (2 * 1);
            }
        }

        [GeneratedRegex("(\\d)+")]
        private static partial Regex RaceParser();
    }
}
