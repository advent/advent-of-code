﻿using System.Text.RegularExpressions;

namespace AdventOfCode {
    public static partial class Day2 {
        private static List<Game> BuildGames() {
            string[] lines = File.ReadAllLines("input/Day2");
            List<Game> Games = [];

            foreach (string line in lines) {
                var gamesplit = line.Split(":");
                var g = new Game {
                    Index = int.Parse(GameIndexRegex().Match(gamesplit[0]).Groups[1].Value)
                };
                foreach (string pullChunk in gamesplit[1].Trim().Split(";")) {
                    var p = new Pull();
                    MatchCollection cubeMatch = CubeCountRegex().Matches(pullChunk);
                    foreach (Match cubeChunk in cubeMatch.Cast<Match>()) {
                        var c = new Cubes {
                            Amount = int.Parse(cubeChunk.Groups[1].Value),
                            Type = Enum.Parse<CubeType>(cubeChunk.Groups[2].Value)
                        };
                        p.CubeList.Add(c);
                    }
                    g.Pulls.Add(p);
                }
                Games.Add(g);
            }

            return Games;
        }

        public static int Day2_1() {
            List<Game> Games = BuildGames();
            List<Cubes> Limiters = [
                new Cubes() { Amount = 12, Type = CubeType.red },
                new Cubes() { Amount = 13, Type = CubeType.green },
                new Cubes() { Amount = 14, Type = CubeType.blue }
            ];

            var viableGames = Games.Where(g => {
                return !Limiters.Any(l => g.Pulls.Any(p => p.CubeList.Any(cl => cl.Type == l.Type && cl.Amount > l.Amount)));
            });

            return viableGames.Select(x => x.Index).Sum();
        }

        public static int Day2_2() {
            List<Game> Games = BuildGames();

            foreach (var g in Games) {
                foreach (CubeType t in Enum.GetValues(typeof(CubeType))) {
                    var min = g.Pulls.Select(x => {
                        var cl = x.CubeList.Where(c => c.Type == t).Select(c => c.Amount);
                        if (cl.Any()) {
                            return cl.Max();
                        }
                        return 0;
                    }).Max();
                    g.Min.Add(new Cubes {
                        Type = t,
                        Amount = min
                    });
                }
            }

            return Games.Select(g => {
                var total = 1;
                foreach (var a in g.Min) {
                    total *= a.Amount;
                }
                return total;
            }).Sum();
        }

        [GeneratedRegex(@"(\d{1,2}) (\w+)")]
        private static partial Regex CubeCountRegex();
        [GeneratedRegex(@"(\d{1,3})")]
        private static partial Regex GameIndexRegex();
    }

    public class Game {
        public int Index { get; set; }
        public List<Pull> Pulls { get; set; } = [];

        public List<Cubes> Min { get; set; } = [];
    }

    public class Pull {
        public List<Cubes> CubeList { get; set; } = [];

    }

    public class Cubes {
        public int Amount { get; set; }
        public CubeType Type { get; set; }
    }

    public enum CubeType {
        green,
        blue,
        red,
    }
}
