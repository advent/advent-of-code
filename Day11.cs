﻿namespace AdventOfCode {
    /// <summary>
    /// It's just pathfinding bro
    /// </summary>
    public static class Day11 {
        public static long Day11_1() => Navigate(2);
        public static long Day11_2() => Navigate(1000000);

        private static long Navigate(long multiplier) {
            var locations = LoadData();
            var galaxies = locations.Values.Where(x => x.Galaxy).ToList();
            long count = 0;
            for (int i = 0; i < galaxies.Count; i++) {
                for (int j = i + 1; j < galaxies.Count; j++) {
                    Location current = new(){ x = galaxies[i].x, y = galaxies[i].y };
                    while (!(current.x == galaxies[j].x && current.y == galaxies[j].y)) {
                        if (current.x != galaxies[j].x) {
                            current.x += galaxies[j].x > current.x ? 1 : -1;
                        } else if (current.y != galaxies[j].y) {
                            current.y += galaxies[j].y > current.y ? 1 : -1;
                        }
                        count += locations[(current.x, current.y)].Empty ? multiplier : 1;
                    }
                }
            }
            return count;
        }

        private static Dictionary<(int, int), Location> LoadData() {
            List<string> lines = [.. File.ReadAllLines("input/Day11")];

            Dictionary<(int, int), Location> Locations = [];

            for (int y = 0; y < lines.Count; y++) {
                for (int x = 0; x < lines[0].Length; x++) {
                    Locations.Add((x, y), new Location {
                        x = x,
                        y = y,
                        Galaxy = lines[y][x] == '#',
                        Empty = (!lines.Where(l => l[x] == '#').Any() || !lines[y].Contains('#'))
                    });
                }
            }

            return Locations;
        }

        public class Location {
            public bool Galaxy;
            public bool Empty;

            public int x;
            public int y;
        }
    }
}
