﻿using System.Text.RegularExpressions;

namespace AdventOfCode {
    /// <summary>
    /// It's just GCF bro
    /// </summary>
    public static class Day8 {
        public static long Day8_1() {
            var (directions, Locations) = LoadCoordinates();

            var loc = "AAA";
            int i = 0;
            while(loc != "ZZZ") {
                loc = Locations[loc + directions[i % directions.Count]];
                i++;
            }

            return i;
        }
        public static long Day8_2() {
            var (directions, Locations) = LoadCoordinates();

            var AList = Locations.Where(x => x.Key.EndsWith("AL")).Select(x => x.Key.Substring(0,3)).ToList();
            Dictionary<int, int> keyValuePairs = new();

            for (int i = 0; i < AList.Count(); i++) {
                for(int j = 0; j < int.MaxValue; j++) {
                    AList[i] = Locations[AList[i] + directions[j % directions.Count]];
                    if (AList[i][2] == 'Z') {
                        keyValuePairs.Add(i, j + 1);
                        break;
                    }
                }
            }
            long biggest = GCD(keyValuePairs[0], keyValuePairs[1]);
            long count = biggest;
            foreach(var kvp in keyValuePairs) {
                count *= kvp.Value / biggest;
            }
            
            return count;
        }

        private static long GCD(long a, long b) {
            while (a != 0 && b != 0) {
                if (a > b)
                    a %= b;
                else
                    b %= a;
            }

            return a | b;
        }

        private static (List<string>, Dictionary<string, string>) LoadCoordinates() {
            string[] lines = File.ReadAllLines("input/Day8");
            List<string> directions = lines[0].ToArray().Select(x => x.ToString()).ToList();
            Regex lineRegex = new("([A-Z0-9]{3}) = \\(([A-Z0-9]{3}), ([A-Z0-9]{3})\\)");
            Dictionary<string, string> Locations = new();
            for(int i = 2; i < lines.Length; i++) {
                var m = lineRegex.Match(lines[i]);
                Locations.Add(m.Groups[1].Value + "L", m.Groups[2].Value);
                Locations.Add(m.Groups[1].Value + "R", m.Groups[3].Value);
            }

            return (directions, Locations);
        }
    }
}
