﻿namespace AdventOfCode {
    /// <summary>
    /// It's just gaps and valleys bro
    /// </summary>
    public static class Day5 {
        public static long Day5_1() {
            (List<Seed> seeds, List<Map> maps) = BuildData("1");
            foreach (var seed in seeds) {
                seed.SoilId = GetMappedItem(seed.SeedId, maps, MapType.Soil);
                seed.FertilizerId = GetMappedItem(seed.SoilId, maps, MapType.Fertilizer);
                seed.WaterId = GetMappedItem(seed.FertilizerId, maps, MapType.Water);
                seed.LightId = GetMappedItem(seed.WaterId, maps, MapType.Light);
                seed.TemperatureId = GetMappedItem(seed.LightId, maps, MapType.Temperature);
                seed.HumidityId = GetMappedItem(seed.TemperatureId, maps, MapType.Humidity);
                seed.LocationId = GetMappedItem(seed.HumidityId, maps, MapType.Location);
            }
            return seeds.OrderBy(x => x.LocationId).Select(x => x.LocationId).First();
        }

        public static long Day5_2() {
            (List<Seed> seeds, List<Map> maps) = BuildData("2");
            foreach (var seed in seeds) {
                var Soils = GetRanges(new Range(seed.SeedId, seed.SeedId + seed.SeedRange - 1), maps, MapType.Soil);
                //Get ranges for each range, combine them
                var Fertilizers = Soils.Select(x => GetRanges(x, maps, MapType.Fertilizer)).SelectMany(x => x).Distinct().ToList();
                var Waters = Fertilizers.Select(x => GetRanges(x, maps, MapType.Water)).SelectMany(x => x).Distinct().ToList();
                var Lights = Waters.Select(x => GetRanges(x, maps, MapType.Light)).SelectMany(x => x).Distinct().ToList();
                var Temperatures = Lights.Select(x => GetRanges(x, maps, MapType.Temperature)).SelectMany(x => x).Distinct().ToList();
                var Humidities = Temperatures.Select(x => GetRanges(x, maps, MapType.Humidity)).SelectMany(x => x).Distinct().ToList();
                var Locations = Humidities.Select(x => GetRanges(x, maps, MapType.Location)).SelectMany(x => x).Distinct().ToList();

                seed.LocationId = Locations.OrderBy(x => x.Min).First().Min;
            }
            return seeds.OrderBy(x => x.LocationId).Select(x => x.LocationId).First();
        }

        private static List<Range> GetRanges(Range item, List<Map> maps, MapType mapType) {
            //Get maps that matter
            var filteredMaps = maps.Where(x =>
                x.MapType == mapType &&
                x.SourceStart <= item.Max
            ).OrderBy(x => x.SourceStart);

            List<Range> ranges = [];
            long i = item.Min;
            while (i < item.Max) {
                var f = filteredMaps.OrderBy(x => x.SourceStart).FirstOrDefault(x => x.SourceStart + x.Range - 1 > i);
                if (f != null) {
                    //Fill the gap before a matched range
                    if (i < f.SourceStart) {
                        ranges.Add(new Range(i, f.SourceStart - 1));
                        i = f.SourceStart;
                    }
                    //Fill the matched range
                    ranges.Add(new Range(i - f.SourceStart + f.DestinationStart, Math.Min(item.Max, f.SourceStart + f.Range) - f.SourceStart + f.DestinationStart));
                    i = Math.Min(item.Max, f.SourceStart + f.Range - 1);
                } else if (f == null) {
                    //No ranges are 1 -> 1
                    ranges.Add(new Range(i, item.Max));
                    i = item.Max + 1;
                }
            }

            return ranges;
        }

        private static long GetMappedItem(long SourceId, List<Map> maps, MapType mapType) {
            var f = maps.FirstOrDefault(m => m.MapType == mapType && m.SourceStart <= SourceId && m.SourceStart + m.Range - 1 >= SourceId);
            if (f == null) {
                return SourceId;
            }
            return SourceId - f.SourceStart + f.DestinationStart;
        }

        private static (List<Seed>, List<Map>) BuildData(string type) {
            string[] lines = File.ReadAllLines("input/Day5");
            MapType mapType = MapType.Soil;
            List<Seed> seeds = [];
            List<Map> maps = [];
            foreach (string line in lines) {
                if (line.Length == 0) {
                    continue;
                } else if (line.StartsWith("seeds:")) {
                    if (type == "1") {
                        seeds = line.Replace("seeds: ", "").Trim().Split(" ").Select(x => new Seed {
                            SeedId = long.Parse(x),
                            SeedRange = 1
                        }).ToList();
                    } else if (type == "2") {
                        var nums = line.Replace("seeds: ", "").Trim().Split(" ");
                        long seedId = 0;
                        for (long i = 0; i < nums.Length; i++) {
                            if (i % 2 != 0) {
                                seeds.Add(new Seed {
                                    SeedId = seedId,
                                    SeedRange = long.Parse(nums[1])
                                });
                            } else {
                                seedId = long.Parse(nums[i]);
                            }
                        }
                    }
                } else if (line.StartsWith("seed-to-soil map:")) {
                    mapType = MapType.Soil;
                } else if (line.StartsWith("soil-to-fertilizer map:")) {
                    mapType = MapType.Fertilizer;
                } else if (line.StartsWith("fertilizer-to-water map:")) {
                    mapType = MapType.Water;
                } else if (line.StartsWith("water-to-light map:")) {
                    mapType = MapType.Light;
                } else if (line.StartsWith("light-to-temperature map:")) {
                    mapType = MapType.Temperature;
                } else if (line.StartsWith("temperature-to-humidity map:")) {
                    mapType = MapType.Humidity;
                } else if (line.StartsWith("humidity-to-location map:")) {
                    mapType = MapType.Location;
                } else {
                    var pieces = line.Split(" ");
                    maps.Add(new Map {
                        MapType = mapType,
                        SourceStart = long.Parse(pieces[1]),
                        DestinationStart = long.Parse(pieces[0]),
                        Range = long.Parse(pieces[2])
                    });
                }
            }

            return (seeds, maps);
        }
        public class Range {
            public long Min { get; }
            public long Max { get; }
            public Range(long min, long max) => (Min, Max) = (min, max);
        }

        public class Seed {
            public long SeedId;
            public long SeedRange;
            public long SoilId;
            public long FertilizerId;
            public long WaterId;
            public long LightId;
            public long TemperatureId;
            public long HumidityId;
            public long LocationId;
        }
        public class Map {
            public MapType MapType;
            public long SourceStart;
            public long DestinationStart;
            public long Range;
        }

        public enum MapType {
            Soil,
            Fertilizer,
            Water,
            Light,
            Temperature,
            Humidity,
            Location
        }
    }
}
