﻿using System.Text.RegularExpressions;

namespace AdventOfCode {

    public static partial class Day4 {
        public static int Day4_1() {
            List<Card> cards = GenerateCards();

            var total = 0;

            foreach(var card in cards) {
                var subtotal = 0;

                foreach(var winner in card.YourIds.Where(x => card.WinningIds.Contains(x))) {
                    if (subtotal == 0) {
                        subtotal = 1;
                    } else {
                        subtotal *= 2;
                    }
                }

                total += subtotal;
            }

            return total;
        }

        public static int Day4_2() {
            List<Card> cards = GenerateCards();
            int total = 0;
            foreach(var card in cards) {
               GetWinners(card, cards.Count);
            }
            foreach(var card in cards) {
                total += GetCounts(card, cards);
            }

            return total;
        }

        private static int GetCounts(Card card, List<Card> cards) {
            if (card.subtotal == -1) {
                int subtotal = cards
                    .Where(x => card.WonCards.Contains(x.Id))
                    .Select(x => GetCounts(x, cards))
                    .Sum();
                card.subtotal = subtotal + 1;
            }
            return card.subtotal;

        }

        private static void GetWinners(Card card, int totalCards) {
            var winners = card.YourIds.Where(x => card.WinningIds.Contains(x));
            for (int windex = 0; windex < winners.Count(); windex++) {
                if (card.Id + windex > totalCards) break;
                card.WonCards.Add(card.Id + windex + 1);
            }
        }

        private static List<Card> GenerateCards() {
            string[] lines = File.ReadAllLines("input/Day4");
            List<Card> cards = [];
            Regex cardLine = LineRegex();

            foreach (string line in lines) {
                Card card = new();
                var lineMatch = cardLine.Match(line);
                card.Id = int.Parse(lineMatch.Groups[1].Value.Trim()) - 1;
                card.WinningIds = lineMatch.Groups[2].Value.Trim().Replace("  ", " ").Split(" ").Select(x => int.Parse(x.Trim())).ToList();
                card.YourIds = lineMatch.Groups[3].Value.Trim().Replace("  ", " ").Split(" ").Select(x => int.Parse(x.Trim())).ToList();
                cards.Add(card);
            }

            return cards;
        }

        public class Card{
            public int Id { get; set; }
            public List<int> WinningIds { get; set; } = [];
            public List<int> YourIds { get; set; } = [];

            public List<int> WonCards { get; set; } = [];
            public int subtotal = -1;
        }

        [GeneratedRegex("(?:Card\\s{1,3})(\\d{1,3})(?:\\:)((?: {1,2}\\d{1,2})+)(?: \\|)((?: {1,2}\\d{1,2})+)")]
        private static partial Regex LineRegex();
    }
}
