﻿namespace AdventOfCode {
    /// <summary>
    /// It's just absolutely fucked bro
    /// </summary>
    public static class Day10 {
        public static long Day10_1() {
            var TileMap = LoadData();
            var start = TileMap.First(x => x.Value.Start).Value;
            Tile? last = null;
            Tile current = start;
            int count = 0;
            
            while (count == 0 || current.Start != true) {
                var f = GetNext(TileMap, last, current);
                last = current;
                current = f;
                count++;
            }
            return count / 2;
        }
        private static Tile GetNext(Dictionary<(int, int), Tile> TileMap, Tile? Last, Tile Current) {
            return TileMap.First(x =>
                ((Current.N && x.Value.S && x.Value.y == Current.y - 1 && x.Value.x == Current.x) ||
                (Current.S && x.Value.N && x.Value.y == Current.y + 1 && x.Value.x == Current.x) ||
                (Current.E && x.Value.W && x.Value.x == Current.x + 1 && x.Value.y == Current.y) ||
                (Current.W && x.Value.E && x.Value.x == Current.x - 1 && x.Value.y == Current.y)) &&
                !(x.Value.x == Last?.x && x.Value.y == Last?.y)
            ).Value;
        }
        public static long Day10_2() {
            var TileMap = LoadDataExpanded();
            var start = TileMap.First(x => x.Value.Start).Value;
            Tile? last = null;
            Tile current = start;
            int count = 0;

            while (count == 0 || current.Start != true) {
                var f = GetNext(TileMap, last, current);
                f.Loop = true;
                last = current;
                current = f;
                count++;
            }

            List<Tile> Fill = [];
            Fill.Add(TileMap.First().Value);
            Fill.First().Filled = true;
            while(Fill.Count > 0) {
                var fills = TileMap.Where(x =>
                    ((x.Value.y == Fill[0].y - 1 && x.Value.x == Fill[0].x) ||
                    (x.Value.y == Fill[0].y + 1 && x.Value.x == Fill[0].x) ||
                    (x.Value.x == Fill[0].x + 1 && x.Value.y == Fill[0].y) ||
                    (x.Value.x == Fill[0].x - 1 && x.Value.y == Fill[0].y)) &&
                    x.Value.Filled == false && x.Value.Loop == false
                ).Select(x => x.Value);
                Fill.ForEach(x => x.Filled = true);
                Fill.AddRange(fills);
                Fill.RemoveAt(0);
            }

            for (int y = 0; y < TileMap.Keys.Select(x => x.Item2).Max(); y++) {
                Console.WriteLine();
                for (int x = 0; x < TileMap.Keys.Select(x => x.Item1).Max(); x++) {
                    if (TileMap[(x, y)].Loop) {
                        Console.ForegroundColor = ConsoleColor.Green;
                    } else if (TileMap[(x, y)].Filled) {
                        Console.ForegroundColor = ConsoleColor.Cyan;
                    }  else {
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    Console.Write(TileMap[(x,y)].c);
                }
            }
            Console.WriteLine();

            return TileMap.Where(x => x.Value.Loop == false && x.Value.Filled == false && x.Value.c != "x").Count();
        }

        private static Dictionary<(int, int), Tile> LoadData() {
            string[] lines = File.ReadAllLines("input/Day10");
            Dictionary<(int, int), Tile> Tiles = [];
            for(int y = 0; y < lines.Length; y++) {
                for(int x = 0; x < lines[y].Length; x++) {
                    Tiles.Add((x, y), new Tile(lines[y][x].ToString(), x, y));
                }
            }
            var start = Tiles.First(x => x.Value.Start).Value;
            start.N = Tiles[(start.x, start.y - 1)].S;
            start.S = Tiles[(start.x, start.y + 1)].N;
            start.E = Tiles[(start.x + 1, start.y)].W;
            start.W = Tiles[(start.x - 1, start.y)].E;
            return Tiles;
        }
        private static Dictionary<(int, int), Tile> LoadDataExpanded() {
            string[] lines = File.ReadAllLines("input/Day10");
            Dictionary<(int, int), Tile> Tiles = [];
            for (int y = 0; y < lines.Length; y++) {
                for (int x = 0; x < lines[y].Length; x++) {
                    Tiles.Add((x * 2, y * 2), new Tile(lines[y][x].ToString(), x * 2, y * 2));
                    Tiles.Add((x * 2 + 1, y * 2), new Tile("x", x * 2 + 1, y * 2, true));
                }
                for (int x = 0; x <= Tiles.Keys.Select(x => x.Item1).Max(); x++) {
                    Tiles.Add((x, y * 2 + 1), new Tile("x", x, y * 2 + 1, true));
                }
            }
            var start = Tiles.First(x => x.Value.Start).Value;
            start.N = Tiles[(start.x, start.y - 2)].S;
            start.S = Tiles[(start.x, start.y + 2)].N;
            start.E = Tiles[(start.x + 2, start.y)].W;
            start.W = Tiles[(start.x - 2, start.y)].E;

            int MaxX = Tiles.Keys.Select(x => x.Item1).Max();
            int MaxY = Tiles.Keys.Select(x => x.Item2).Max();

            for (int y = 0; y < MaxY; y++) {
                for (int x = 0; x < MaxX; x++) {
                    if (Tiles[(x, y)].Filler) {
                        Tiles[(x, y)].W = Tiles[(Math.Max(x - 1, 0), y)].E;
                        Tiles[(x, y)].E = Tiles[(Math.Min(x + 1, MaxX), y)].W;
                        Tiles[(x, y)].N = Tiles[(x, Math.Max(y - 1, 0))].S;
                        Tiles[(x, y)].S = Tiles[(x, Math.Min(y + 1, MaxY))].N;
                    }
                }
            }
            return Tiles;
        }

        public class Tile {
            public bool N;
            public bool E;
            public bool S;
            public bool W;
            public bool Start;
            public bool Loop;
            public bool Filler;
            public bool Filled;

            public int x;
            public int y;
            public string c;

            public Tile(string tile, int x, int y, bool filler = false) {
                this.x = x;
                this.y = y;
                this.c = tile;
                this.Filler = filler;
                
                if (tile == "|") {
                    N = true;
                    S = true;
                } else if (tile == "-") {
                    E = true;
                    W = true;
                } else if (tile == "L") {
                    N = true;
                    E = true;
                } else if (tile == "J") {
                    W = true;
                    N = true;
                } else if (tile == "7") {
                    W = true;
                    S = true;
                } else if (tile == "F") {
                    E = true;
                    S = true;
                } else if (tile == "S") {
                    Start = true;
                }
            }
        }
    }
}
