﻿using System.Text.RegularExpressions;

namespace AdventOfCode {
    /// <summary>
    /// It's just hitboxes bro
    /// </summary>
    public static partial class Day3 {
        public static int Day3_1() {
            (List<Hitbox> hits, List<Hitbox> symbols) = BuildHitboxes(@"([^\d|\.])");
            var total = 0;
            foreach (Hitbox hit in hits) {
                var h = symbols.Where(h => {
                    return h.x2 >= hit.x && h.x <= hit.x2 && h.y <= hit.y2 && h.y2 >= hit.y;
                });
                if (h.Any()) {
                    total += hit.value;
                }
            }

            return total;
        }

        public static int Day3_2() {
            (List<Hitbox> hits, List<Hitbox> gears) = BuildHitboxes(@"(\*)");

            var total = 0;
            foreach (Hitbox gear in gears) {
                var h = hits.Where(h => {
                    return h.x2 >= gear.x && h.x <= gear.x2 && h.y <= gear.y2 && h.y2 >= gear.y;
                });
                if (h.Count() >= 2) {
                    var subtotal = 1;
                    foreach (var item in h) {
                        subtotal *= item.value;
                    }
                    total += subtotal;
                }
            }

            return total;
        }

        private static (List<Hitbox>, List<Hitbox>) BuildHitboxes(string EnemyRegex) {
            string[] lines = File.ReadAllLines("input/Day3");
            List<Hitbox> hits = [];
            List<Hitbox> enemies = [];
            Regex enemy = new(EnemyRegex);
            for (int y = 0; y < lines.Length; y++) {
                int start = -1;
                int end = -1;
                string num = "";
                for (int x = 0; x < lines[0].Length; x++) {

                    if (NumberMatch().IsMatch(lines[y][x].ToString())) {
                        if (start == -1) {
                            start = x;
                            end = x;
                            num += lines[y][x];
                        } else {
                            end = x;
                            num += lines[y][x];
                            if (x == lines[y].Length - 1) {
                                hits.Add(new Hitbox(start, end, y, y, int.Parse(num)));
                                start = -1;
                                end = -1;
                                num = "";
                            }
                        }
                    } else if (start > -1) {
                        hits.Add(new Hitbox(start, end, y, y, int.Parse(num)));
                        start = -1;
                        end = -1;
                        num = "";
                    }
                    if (enemy.IsMatch(lines[y][x].ToString())) {
                        enemies.Add(new Hitbox(
                            Math.Max(x - 1, 0),
                            Math.Min(x + 1, lines[0].Length - 1),
                            Math.Max(y - 1, 0),
                            Math.Min(y + 1, lines.Length - 1)
                        ));
                    }
                }
            }

            return (hits, enemies);
        }

        public class Hitbox(int x, int x2, int y, int y2, int value = -1) {
            public int x = x;
            public int x2 = x2;
            public int y = y;
            public int y2 = y2;
            public int value = value;
        }

        [GeneratedRegex(@"([^\d|\.])")]
        private static partial Regex SymbolSearch();
        [GeneratedRegex(@"(\d)")]
        private static partial Regex NumberMatch();
    }
}
